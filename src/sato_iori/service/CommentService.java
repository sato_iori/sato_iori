package sato_iori.service;

import static sato_iori.utils.CloseableUtil.*;
import static sato_iori.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import sato_iori.beans.Comment;
import sato_iori.beans.MessageComment;
import sato_iori.dao.CommentDao;
import sato_iori.dao.MessageCommentDao;

public class CommentService {
	public void register(Comment comment) {

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.insert(connection, comment);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	private static final int LIMIT_NUM = 1000;

	public List<MessageComment> getComment() {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageCommentDao CommentDao = new MessageCommentDao();
			List<MessageComment> ret = CommentDao.getMessageComments(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(String commentId) {
		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.delete(connection, commentId);
			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}
}
