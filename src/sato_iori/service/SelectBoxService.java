package sato_iori.service;

import static sato_iori.utils.CloseableUtil.*;
import static sato_iori.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import sato_iori.beans.SelectBox;
import sato_iori.dao.SelectBoxDao;

public class SelectBoxService {
	public List<SelectBox> getAffiliationList() {

		Connection connection = null;
		try {
			connection = getConnection();

			List<SelectBox> ret = SelectBoxDao.getAffiliations(connection);

			commit(connection);
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<SelectBox> getBranchList() {

		Connection connection = null;
		try {
			connection = getConnection();

			List<SelectBox> ret = SelectBoxDao.getBranches(connection);

			commit(connection);
			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
