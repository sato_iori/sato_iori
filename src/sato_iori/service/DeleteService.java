package sato_iori.service;

import static sato_iori.utils.CloseableUtil.*;
import static sato_iori.utils.DBUtil.*;

import java.sql.Connection;

import sato_iori.beans.User;
import sato_iori.dao.IsDeletedDao;

public class DeleteService {
	public void update(User editUser) {
		Connection connection = null;
		try {
			connection = getConnection();

			IsDeletedDao isDeletedDao = new IsDeletedDao();
			isDeletedDao.update(connection, editUser);
			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}


}
