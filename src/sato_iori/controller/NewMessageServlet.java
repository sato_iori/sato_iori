package sato_iori.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import sato_iori.beans.Message;
import sato_iori.beans.User;
import sato_iori.service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
	public static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		request.getRequestDispatcher("message.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		if (isValid(request,messages) == true) {
			User user = (User) session.getAttribute("loginUser");

			Message message = new Message();

			message.setSubject(request.getParameter("subject"));
			message.setText(request.getParameter("message"));
			message.setCategory(request.getParameter("category"));
			message.setUserId(user.getId());

			new MessageService() .register(message);

			response.sendRedirect("./");
		} else {
			String subject = request.getParameter("subject");
			String message = request.getParameter("message");
			String category =request.getParameter("category");

			request.setAttribute("subject", subject);
			request.setAttribute("message", message);
			request.setAttribute("category", category);

			session.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("message.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String subject =request.getParameter("subject");
		String message = request.getParameter("message");
		String category = request.getParameter("category");

		if (StringUtils.isBlank(subject) == true) {
			messages.add("件名を入力してください");
		}
		if (StringUtils.isBlank(message) == true) {
			messages.add("投稿内容を入力してください");
		}
		if (StringUtils.isBlank(category) == true) {
			messages.add("カテゴリーを入力してください");
		}
		if (30 < subject.length()) {
			messages.add("タイトルを30文字以内で入力してください");
		}
		if (1000 < message.length()) {
			messages.add("投稿内容を1000文字以内で入力してください");
		}
		if (10 < category.length()) {
			messages.add("カテゴリーを10文字以内で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
