package sato_iori.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sato_iori.beans.User;
import sato_iori.service.CommentService;

@WebServlet(urlPatterns = { "/commentdelete" })
public class CommentDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		User user = (User) ((HttpServletRequest) request).getSession().getAttribute("loginUser");
		String commentId = request.getParameter("commentId");

		if(Integer.parseInt(request.getParameter("commentUserId")) == user.getId()) {
			new CommentService().delete(commentId);
			response.sendRedirect("./");
		} else {
			List<String> messages = new ArrayList<String>();
			messages.add("あなたの投稿ではありません");
			request.setAttribute("errorMessages", messages);
        	request.getRequestDispatcher("./").forward(request,response);
		}

	}
}
