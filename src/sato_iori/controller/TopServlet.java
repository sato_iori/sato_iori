package sato_iori.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sato_iori.beans.MessageComment;
import sato_iori.beans.UserMessage;
import sato_iori.service.CommentService;
import sato_iori.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	public static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException {

		List<UserMessage> messages = new MessageService().getMessage(request);
		List<MessageComment> comments = new CommentService().getComment();

		request.setAttribute("messages", messages);
		request.setAttribute("comments", comments);

		String categorySearch = request.getParameter("categorySearch");
		String dateSearch = request.getParameter("dateSearch");
		String dateSearch2 = request.getParameter("dateSearch_2");

		request.setAttribute("categorySearch", categorySearch);
		request.setAttribute("dateSearch", dateSearch);
		request.setAttribute("dateSearch2", dateSearch2);

		request.getRequestDispatcher("/top.jsp").forward(request,response);
	}

}
