package sato_iori.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import sato_iori.beans.SelectBox;
import sato_iori.beans.User;
import sato_iori.beans.UserInfo;
import sato_iori.exception.NoRowsUpdatedRuntimeException;
import sato_iori.service.SelectBoxService;
import sato_iori.service.UserService;

@WebServlet(urlPatterns = { "/settings" })
@MultipartConfig(maxFileSize = 100000)
public class SettingsServlet  extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<SelectBox> affiliationList = new SelectBoxService().getAffiliationList();
		List<SelectBox> branchList = new SelectBoxService().getBranchList();
		List<UserInfo> users = new UserService().getUserList();
		HttpSession session = request.getSession();

		if(StringUtils.isBlank(request.getParameter("userId"))) {
			List<String> messages = new ArrayList<String>();
			messages.add("存在しないユーザーです");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
			return;
		}

		if(!StringUtils.isNumeric(request.getParameter("userId")) ) {
			List<String> messages = new ArrayList<String>();
			messages.add("存在しないユーザーです");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
			return;
		}
		if(users.size() < (Integer.parseInt(request.getParameter("userId")))) {
			List<String> messages = new ArrayList<String>();
			messages.add("存在しないユーザーです");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
			return;
		}
		User editUser = new UserService().getUser(Integer.parseInt(request.getParameter("userId")));
		request.setAttribute("editUser", editUser);
		session.setAttribute("affiliationList", affiliationList);
		session.setAttribute("branchList", branchList);
		request.getRequestDispatcher("settings.jsp" ).forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();

		User editUser = getEditUser(request);

		request.setAttribute("editUser", editUser);
		if (isValid(request, messages) == true) {
			try {
				new UserService().update(editUser);
			} catch (NoRowsUpdatedRuntimeException e) {
				request.removeAttribute("editUser");
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認して下さい。");
				request.setAttribute("errorMesssages", messages);
				request.getRequestDispatcher("settings.jsp").forward(request, response);
				return;
			}
			request.setAttribute("editUser", editUser);

			response.sendRedirect("management");

		} else {

			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request) throws IOException, ServletException {


		User editUser = new User();

		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLoginId(request.getParameter("loginId"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setName(request.getParameter("name"));
		editUser.setBranchId(Integer.parseInt(request.getParameter("branch")));
		editUser.setAffiliation(Integer.parseInt(request.getParameter("affiliation")));
		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password_2");
		String name = request.getParameter("name");
		int branchId = Integer.parseInt(request.getParameter("branch"));
		int affiliationId = Integer.parseInt(request.getParameter("affiliation"));
		User user = (User) request.getSession().getAttribute("loginUser");
		User users = new UserService().getUser(loginId);


		if (StringUtils.isBlank(loginId) == true) {
			messages.add("ログインIDを入力してください");
		} else if (loginId.length() < 6 || loginId.length() > 20) {
			messages.add("ログインIDは6文字以上20文字以下で入力してください");
		}
		if(users != null) {

			if(users.getId() != Integer.parseInt(request.getParameter("id"))) {
				if(users.getLoginId().equals(loginId)) {
					messages.add("このログインIDは使用できません");
				}
			}

		}

		if(!StringUtils.isBlank(password)) {
			if (password.length() < 6 || password.length() > 20) {
				messages.add("パスワードは6文字以上20文字以下で入力してください");
			}
			if (!password.equals(password2)) {
				messages.add("同じパスワードを入力してください");
			}
		}
		if(!StringUtils.isBlank(password2)) {
			if (StringUtils.isBlank(password)) {
				messages.add("パスワードを入力してください");
			}
		}
		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		} else if (10 < name.length()) {
			messages.add("名前は10文字以下で入力してください");
		}
		if (branchId < 1) {
			messages.add("支店を選択してください");
		}
		if(branchId == 1 && (affiliationId == 3 || affiliationId == 4)) {
			messages.add("選択中の支店名では、選択した部署・役職を選択できません");
		}
		if(branchId != 1 && (affiliationId == 1 || affiliationId == 2)) {
			messages.add("選択中の支店名では、選択した部署・役職を選択できません");
		}
		if(affiliationId != 1) {
			if (affiliationId == 1 && user.getLoginId().equals(loginId)) {
				messages.add("自分では変更できません");
			}
		}

		//TODO アカウントがすでに利用されていないか、メールアドレスがすでに登録されていないかどうかなどの確認も必要
		if(messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
