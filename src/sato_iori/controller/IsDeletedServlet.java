package sato_iori.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sato_iori.beans.User;
import sato_iori.service.DeleteService;
import sato_iori.service.UserService;

@WebServlet(urlPatterns = { "/isdeleted" })
public class IsDeletedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		User user = (User) ((HttpServletRequest) request).getSession().getAttribute("loginUser");
		User editUser = new UserService().getUser(Integer.parseInt(request.getParameter("userId")));

		if(user.getId() == editUser.getId()) {
			List<String> messages = new ArrayList<String>();
			messages.add("自分のアカウントは停止できません");
			request.setAttribute("errorMessages", messages);

		} else {
			if(editUser.getIsDeleted() == 0) {
			editUser.setIsDeleted(1);
			new DeleteService().update(editUser);
			} else {
			editUser.setIsDeleted(0);
			new DeleteService().update(editUser);
			}
			response.sendRedirect("management");
			return;
		}
		request.getRequestDispatcher("management").forward(request, response);
	}
}