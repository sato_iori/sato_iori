package sato_iori.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import sato_iori.beans.SelectBox;
import sato_iori.beans.User;
import sato_iori.service.SelectBoxService;
import sato_iori.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		List<SelectBox> affiliationList = new SelectBoxService().getAffiliationList();
		List<SelectBox> branchList = new SelectBoxService().getBranchList();
		HttpSession session = request.getSession();

		session.setAttribute("affiliationList", affiliationList);
		session.setAttribute("branchList", branchList);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		List<String> messages = new ArrayList<String>();


		if (isValid(request, messages) == true) {

			User user = new User();
			user.setLoginId(request.getParameter("loginId"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranchId(Integer.parseInt(request.getParameter("branch")));
			user.setAffiliation(Integer.parseInt(request.getParameter("affiliation")));

			new UserService().register(user);

			response.sendRedirect("management");
		} else {
			request.setAttribute("errorMessages", messages);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password_2");
		String name = request.getParameter("name");
		int branchId = Integer.parseInt(request.getParameter("branch"));
		int affiliationId = Integer.parseInt(request.getParameter("affiliation"));

		request.setAttribute("loginId", loginId);
		request.setAttribute("name", name);
		request.setAttribute("branchId", branchId);
		request.setAttribute("affiliationId", affiliationId);

		User users = new UserService().getUser(loginId);

		if (StringUtils.isBlank(loginId) == true) {
			messages.add("ログインIDを入力してください");
		} else if (loginId.length() < 6 || loginId.length() > 20) {
			messages.add("ログインIDは6文字以上20文字以下で入力してください");
		}
		if(users != null) {
			if(users.getLoginId().equals(loginId)) {
				messages.add("このログインIDは使用できません");
			}
		}

		if (StringUtils.isBlank(password) == true || StringUtils.isBlank(password2) == true) {
			messages.add("パスワードを入力してください");
		} else if (password.length() < 6 || password.length() > 20) {
			messages.add("パスワードは6文字以上20文字以下で入力してください");
		}
		if (!password.equals(password2)) {
			messages.add("同じパスワードを入力してください");
		}
		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		} else if (10 < name.length()) {
			messages.add("名前は10文字以下で入力してください");
		}
		if (branchId < 1) {
			messages.add("支店を選択してください");
		}
		if(branchId == 1 && (affiliationId == 3 || affiliationId == 4)) {
			messages.add("選択中の支店名では、選択した部署・役職を選択できません");
		}
		if(branchId != 1 && (affiliationId == 1 || affiliationId == 2)) {
			messages.add("選択中の支店名では、選択した部署・役職を選択できません");
		}
		if (affiliationId < 1) {
			messages.add("所属を選択してください");
		}

		//TODO アカウントがすでに使用されていないか、メールアドレスがすでに登録されていないかなどの確認も必要
//		if (loginId.contains()))

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
