package sato_iori.dao;

import static sato_iori.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sato_iori.beans.SelectBox;
import sato_iori.exception.SQLRuntimeException;

public class SelectBoxDao {
	public static List<SelectBox> getAffiliations(Connection connection) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM affiliations ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<SelectBox> ret = toAffiliationList(rs);
			return ret;
		} catch (SQLException e){
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private static List<SelectBox> toAffiliationList(ResultSet rs) throws SQLException {
		List<SelectBox> ret = new ArrayList<SelectBox>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int affiliationId = rs.getInt("affiliation_id");
				String affiliationName = rs.getString("name");


				SelectBox selectBox = new SelectBox();
				selectBox.setId(id);
				selectBox.setAffiliationId(affiliationId);
				selectBox.setAffiliationName(affiliationName);

				ret.add(selectBox);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public static List<SelectBox> getBranches(Connection connection) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM branches ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<SelectBox> ret = toBranchList(rs);
			return ret;
		} catch (SQLException e){
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private static List<SelectBox> toBranchList(ResultSet rs) throws SQLException {
		List<SelectBox> ret = new ArrayList<SelectBox>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int branchId = rs.getInt("branch_id");
				String branchName = rs.getString("name");


				SelectBox selectBox = new SelectBox();
				selectBox.setId(id);
				selectBox.setBranchId(branchId);
				selectBox.setBranchName(branchName);

				ret.add(selectBox);
			}
			return ret;
		} finally {
			close(rs);
		}
	}


}
