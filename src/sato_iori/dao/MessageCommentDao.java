package sato_iori.dao;

import static sato_iori.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import sato_iori.beans.MessageComment;
import sato_iori.exception.SQLRuntimeException;

public class MessageCommentDao {
	public List<MessageComment> getMessageComments(Connection connection, int num) {

		java.sql.PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM messages_comments ");
			sql.append("ORDER BY insert_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<MessageComment> ret = toMessageCommentList(rs);
			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}


	private List<MessageComment> toMessageCommentList(ResultSet rs) throws SQLException {
		List<MessageComment> ret = new ArrayList<MessageComment>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String text = rs.getString("text");
				int userId = rs.getInt("user_id");
				int messageId = rs.getInt("message_id");
				Timestamp insertDate = rs.getTimestamp("insert_date");
				String name = rs.getString("name");

				MessageComment comments = new MessageComment();
				comments.setId(id);
				comments.setText(text);
				comments.setUserId(userId);
				comments.setMessageId(messageId);
				comments.setInsertDate(insertDate);
				comments.setName(name);

				ret.add(comments);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
