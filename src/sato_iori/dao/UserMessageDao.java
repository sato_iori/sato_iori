package sato_iori.dao;

import static sato_iori.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import sato_iori.beans.UserMessage;
import sato_iori.exception.SQLRuntimeException;

public class UserMessageDao {
	public List<UserMessage> getUserMessages(Connection connection, int num, HttpServletRequest request) {

		Date d = new Date();
		SimpleDateFormat d1 = new SimpleDateFormat("yyyy/MM/dd");
		String today = d1.format(d);
		String dateSearch = request.getParameter("dateSearch");
		String dateSearch2 = request.getParameter("dateSearch_2");
		String categorySearch = request.getParameter("categorySearch");

		java.sql.PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users_messages ");

			if(dateSearch == null || dateSearch.isEmpty()) {
				dateSearch = "0000/00/00";
			}
			if(dateSearch2 == null || dateSearch2.isEmpty()) {
				dateSearch2 = today;
			}

			sql.append(" WHERE insert_date >= '" + dateSearch + " 00:00:00" + "' AND insert_date <= '" + dateSearch2 + " 23:59:59" + "' ");


			if(StringUtils.isNotBlank(categorySearch)) {
				sql.append(" AND category LIKE '%" + categorySearch + "%' ");
			}
			sql.append("ORDER BY id DESC limit " + num );

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);

		} finally {
			close(ps);
		}
	}


	private List<UserMessage> toUserMessageList(ResultSet rs) throws SQLException {
		List<UserMessage> ret = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String subject = rs.getString("subject");
				String text = rs.getString("text");
				String category = rs.getString("category");
				int userId = rs.getInt("user_id");
				Timestamp insertDate = rs.getTimestamp("insert_date");
				String name = rs.getString("name");

				UserMessage message = new UserMessage();
				message.setId(id);
				message.setSubject(subject);
				message.setText(text);
				message.setCategory(category);
				message.setUserId(userId);
				message.setInsertDate(insertDate);
				message.setName(name);

				ret.add(message);
			}
			return ret;
		} finally {
			close(rs);
		}
	}


}

