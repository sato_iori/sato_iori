package sato_iori.dao;

import static sato_iori.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import sato_iori.beans.User;
import sato_iori.exception.NoRowsUpdatedRuntimeException;

public class IsDeletedDao {
	public void update(Connection connection, User editUser) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("is_deleted = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, editUser.getIsDeleted());
			ps.setInt(2, editUser.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new NoRowsUpdatedRuntimeException();
		} finally {
			close(ps);
		}
	}

}
