package sato_iori.dao;

import static sato_iori.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sato_iori.beans.UserInfo;
import sato_iori.exception.SQLRuntimeException;

public class ManagementDao {
	public static List<UserInfo> getUsers(Connection connection) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM users_info ORDER BY branch_id, affiliation_id");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserInfo> ret = toUserList(rs);
			return ret;
		} catch (SQLException e){
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private static List<UserInfo> toUserList(ResultSet rs) throws SQLException {
		List<UserInfo> ret = new ArrayList<UserInfo>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branchId = rs.getInt("branch_id");
				String branchName = rs.getString("branch_name");
				int affiliationId = rs.getInt("affiliation_id");
				String affiliationName = rs.getString("affiliation_name");
				int isDeleted = rs.getInt("is_deleted");


				UserInfo user = new UserInfo();
				user.setId(id);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setName(name);
				user.setBranchId(branchId);
				user.setBranchName(branchName);
				user.setAffiliationId(affiliationId);
				user.setAffiliationName(affiliationName);
				user.setIsDeleted(isDeleted);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
