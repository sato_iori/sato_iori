package sato_iori.beans;

import java.io.Serializable;

public class SelectBox implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private int branchId;
	private int affiliationId;
	private String branchName;
	private String affiliationName;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAffiliationId() {
		return affiliationId;
	}
	public void setAffiliationId(int affiliationId) {
		this.affiliationId = affiliationId;
	}
	public int getBranchId() {
		return branchId;
	}
	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getAffiliationName() {
		return affiliationName;
	}
	public void setAffiliationName(String affiliationName) {
		this.affiliationName = affiliationName;
	}
}
