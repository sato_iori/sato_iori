package sato_iori.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

import sato_iori.beans.User;


@WebFilter(urlPatterns = {"/management", "/settings", "/signup"})
public class AffiliationCheckFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		User user = (User) ((HttpServletRequest) request).getSession().getAttribute("loginUser");

		if(user == null) {
			chain.doFilter(request, response);
			return;
		}

		if(user.getAffiliation() == 1){
            chain.doFilter(request, response);
        } else {
        	List<String> messages = new ArrayList<String>();
			messages.add("権限がありません");
			request.setAttribute("errorMessages", messages);
        	request.getRequestDispatcher("./").forward(request,response);
        }
	}

	public void init(FilterConfig config) {

	}

	public void destroy() {

	}
}
