package sato_iori.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sato_iori.beans.User;


@WebFilter("/*")
public class LoginCheckFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		HttpSession session = (((HttpServletRequest)request).getSession());
		String servletPath = ((HttpServletRequest)request).getServletPath();

		if(servletPath.equals("/css/style.css")){
		 	chain.doFilter(request, response);
	    	return;

		}
		if(!servletPath.equals("/login") ) {

			User loginUser = (User) session.getAttribute("loginUser");
		    if(loginUser != null){
		    	chain.doFilter(request, response);
		    	return;
		    }
			List<String> messages = new ArrayList<String>();
			messages.add("ログインしてください");
			((HttpSession)session).setAttribute("errorMessages", messages);
			((HttpServletResponse)response).sendRedirect("login");
			return;

		}
		chain.doFilter(request, response);
	}

	public void init(FilterConfig config) {

	}

	public void destroy() {

	}
}
