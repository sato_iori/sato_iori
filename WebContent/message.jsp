<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>新規投稿</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="main-contents">

<div class="header">
		<a href="./">ホーム</a>
		<c:if test="${loginUser.affiliation == 1}">
			<a href="management">ユーザー管理</a>
		</c:if>
		<a href="logout">ログアウト</a>
</div>

<c:if test="${ not empty errorMessages }">
	<div class="errorMessages">
	<div><h3>！以下のエラーが発生しました！</h3></div>
		<ul>
			<c:forEach items="${errorMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session"/>
</c:if>

<div class="from-area">
	<div><h2>新規投稿</h2></div><br />
	<form action="newMessage" method="post">
	件名　(30文字以内で書いてください) <br />
	<input type="text" name="subject" size="60" class="subject-box" value="${subject}">
	<br>投稿内容　(1000文字以内で書いてください) <br />
	<textarea name="message" cols="90" rows="10" class="text-box">${message}</textarea>
	<br />
	カテゴリー　(10文字以内で書いてください)<br />
	<input type="text" name="category" size="20" class="category-box" value="${category}">

	<input class="submit" style="float: right; margin-right: 20%" type="submit" value="投稿" ><br/>

	</form>
</div>


<div class="copyright">Copyright(c)Iori Sato</div>
</div>
</body>
</html>

