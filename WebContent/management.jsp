<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー管理</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents" style="text-align : justify">



	<div class="header">
		<a href="./">ホーム</a>
		<a href="signup">ユーザー新規登録</a>
		<a href="logout">ログアウト</a>
	</div>

<div><h2>ユーザー管理画面</h2></div><br />

<c:if test="${ not empty errorMessages }">
	<div class="errorMessages">
	<div><h3>！以下のエラーが発生しました！</h3></div>
		<ul>
			<c:forEach items="${errorMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session"/>
</c:if>

<table>
<thead>
<tr>
	<th>ログインID</th>
	<th>名前</th>
	<th>支店</th>
	<th>部署・役職</th>
	<th>停止・復活</th>
	<th>各ユーザーの編集</th>
</tr>
</thead>
<tbody>

	<c:forEach items="${users}" var="user">

		<div class="user" style="line-height:200%">
		<form action="isdeleted" method="get">
		<tr>
			<td>${ user.loginId } </td>

			<td>${ user.name } </td>

			<td>${ user.branchName } </td>

			<td>${ user.affiliationName } </td>

			<input type="hidden" name="userId" value="${ user.id }" id="userId" />
			<input type="hidden" name="isDeleted" value="${ user.isDeleted }" id="isDeleted" />
			<td>
			<c:if test="${user.id != loginUser.id }">
				<c:if test="${ user.isDeleted == 0 }"><input style="color: red; background-color: black;" class="isDeleteButton" type="submit" value="停止する" onclick="return confirm('本当に停止しますか？');"></c:if>
				<c:if test="${ user.isDeleted == 1 }"><input style="color: black; background-color: red;" class="isDeleteButton" type="submit" value="復活する" onclick="return confirm('本当に復活しますか？');"></c:if>
			</c:if>
			<c:if test="${user.id == loginUser.id }">　</c:if>
			</td>
			<td><a  href="settings?userId=${ user.id }">編集</a></td>
		</tr>
		</form>
		</div>

	</c:forEach>

</tbody>
</table>


<div class="copyright">Copyright(c)Iori Sato</div>
</div>

</body>
</html>