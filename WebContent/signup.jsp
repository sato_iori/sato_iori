<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー新規登録</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="main-contents">

<div class="header">
		<a href="./">ホーム</a>
		<a href="management">ユーザー管理</a>
		<a href="logout">ログアウト</a>
</div>

<div><h2>ユーザー新規登録</h2></div>

<c:if test="${ not empty errorMessages }">
	<div class="errorMessages">
	<div><h3>！以下のエラーが発生しました！</h3></div>
		<ul>
			<c:forEach items="${errorMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session"/>
</c:if>

<div style="margin-left: 20%">
<form action="signup" method="post"><br />


	<label for="loginId">ログインID(半角英数字6文字以上20文字以内で入力してください)</label>
	<input name="loginId" id="loginId" value="${ loginId }"/>

	<label for="password">パスワード(記号・半角英数字6文字以上20文字以内で入力してください)</label>
	<input name="password" type="password" id="password" /> <br />

	<label for="password_2">パスワード（確認）(同じパスワードをもう一度入力してください)</label>
	<input name="password_2" type="password" id="password_2"/> <br />

	<label for="name">名前(10文字以内で入力してください)</label>
	<input name="name" id="name" value="${ name }"/>

	<label for="branch">支店</label>
	<select name="branch" >
		<c:forEach items="${branchList}" var="branch">
			<option id="branch" ${ branch.branchId == branchId?"selected":"" } value="${ branch.branchId }" >${ branch.branchName }</option>
		</c:forEach>
	</select>

	<label for="affiliation">部署・役職</label>
	<select name="affiliation">
		<c:forEach items="${affiliationList}" var="affiliation">
			<option id="affiliation" ${ affiliation.affiliationId == affiliationId?"selected":"" } value="${ affiliation.affiliationId }" > ${ affiliation.affiliationName }</option>
		</c:forEach>
	</select><br />

	<input class="submit" type="submit" value="登録" />

</form>
</div>
<div class="copyright">Copyright(c)Iori Sato</div>
</div>
</body>
</html>