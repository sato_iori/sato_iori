<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>掲示板</title>
	<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="main-contents">

<div class="header">
		<a href="newMessage">新規投稿</a>
		<c:if test="${loginUser.affiliation == 1}">
			<a href="management">ユーザー管理</a>
		</c:if>
		<a href="logout">ログアウト</a>
</div>

<c:if test="${ not empty errorMessages }">
	<div class="errorMessages">
	<div><h3>！以下のエラーが発生しました！</h3></div>
		<ul>
			<c:forEach items="${errorMessages}" var="message">
				<li><c:out value="${message}" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session"/>
</c:if>

<c:if test="${ not empty loginUser }">
	<div class="profile">ログイン中のユーザー:${ loginUser.name }　@${ loginUser.loginId }

	</div><br/>
</c:if>

<form action="./" method="get">
	<div class="search">
	<span style="font-weight: bolder; font-size: larger;">＜検索＞</span><br />
	<span class="categorySearch">カテゴリー：</span>
	<input type="text" size="20" name="categorySearch" value="${categorySearch}">　
	<span class="dateSearch">投稿日：</span>
	<input type="date" name="dateSearch" value="${dateSearch}">～
	<input type="date" name="dateSearch_2" value="${dateSearch2}"><br />
	<input style="float: right; " class="submit" type="button" value="リセット" onclick="location.href='./'" >
	<input style="float: right;" class="submit" type="submit" value="検索する">
	</div>
</form>

<br />

<div class="messages">
	<c:forEach items="${messages}" var="message">
		<div class="message">
		<form action="messagedelete" method="post">
			<span class="messageId"><c:out value="${ message.id }" />.</span>
			<span class="date" style="float: right;">投稿日時：<fmt:formatDate  value="${ message.insertDate }" pattern="yyyy/MM/dd HH:mm:ss" /></span><br />
			<div class="subject"><c:out value="${ message.subject }" /></div>
			<span class="name" style="float: right;">投稿者：<c:out value="${ message.name }" /></span><br />

			<div class="category" style="float: right;">カテゴリー：<a href="?categorySearch=${ message.category }&dateSearch=&dateSearch_2=">${ message.category }</a></div><br />

			<div class="text"><c:forEach var="str" items="${fn:split(message.text,'
				')}" ><c:out value="${str}" /><br></c:forEach></div><br />
			<c:if test="${ loginUser.id == message.userId }"><input style="float: right;" class="submit"  type="submit" value="投稿を削除する" onclick="return confirm('削除しますか？');"></c:if><br />
			<input type="hidden" name="messageId" value="${ message.id }" id="messageId" />
			<input type="hidden" name="messageUserId" value="${ message.userId }" id="messageUserId" />
		</form>
			<div style="border-bottom: dashed 3px red;">　</div>
			<i class="comment" style="font-size: 24px">コメント一覧</i>
			<c:forEach items="${comments}" var="comment" >
			<form action="commentdelete" method="post">
				<c:if test="${ message.id == comment.messageId }">
					<div class="commentText"><c:forEach var="str" items="${fn:split(comment.text,'
					')}" ><c:out value="${str}" /><br></c:forEach></div>
					<span class="name" >投稿者：${comment.name }</span>
					<span class="date">投稿日時：<fmt:formatDate value="${ comment.insertDate }" pattern="yyyy/MM/dd HH:mm:ss" /></span>
					<c:if test="${ loginUser.id == comment.userId }"><input class="submit" type="submit" value="コメントを削除する" onclick="return confirm('削除しますか？');"></c:if>
					<br />
					<input type="hidden" name="commentId" value="${ comment.id }" id="commentId" />
					<input type="hidden" name="commentUserId" value="${ comment.userId }" id="commentUserId" />
				</c:if>
			</form>
			</c:forEach>
			<div class="form-area">
				<form action="comments" method="post">
					<br>コメント <br />
						<textarea name="text" cols="50" rows="5" class="text-box"  ><c:out value="${ text }" /></textarea>
					<br />
					<input style="margin-left: 38%;" class="submit" type="submit" value="コメントする">(500字まで)
					<input type="hidden" value="${ message.id }" name="messageId" id="messageId" />
				</form>
			</div>
			<div>　</div>

		</div>
	</c:forEach>
</div>
<div class="copyright">Copyright(c)Iori Sato</div>
</div>
</body>
</html>